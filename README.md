﻿<!-- PROJECT SHIELDS -->

[![Stargazers][stars-shield]][stars-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<p align="center">
  <a href="https://gitlab.com/remmsoft-shared/nextapi-net5">
    <img src="nextapi/Assets/images/boilerplate-cover.png" alt="Logo">
  </a>

  <h3 align="center">ASP.NET Core 5 Web API Boilerplate</h3>

  <p align="center">
    <a href="#"><strong>Read More »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/remmsoft-shared/nextapi-net5/-/issues">Want to be a contributor?</a>
  </p>
</p>

## 🚀 Motivation

As the software team, we decided to create a boilerplate by taking the plug-ins and architecture we often use to avoid reinventing the wheel when starting new projects.
We also realized that we often use open source libraries and frameworks and they add value to us. Now it's our turn to take responsibility and we're starting to contribute.

## ℹ️ About the Project
It is a ready-to-use, scalable Web API project that works with the latest .NET Core 5 version, contains useful plugins. It's developer, the REMMSOFT Core team, continues to develop this boilerplate.

### 🧱 Built with

- [ASP.NET Core 5.0](https://dotnet.microsoft.com/learn/aspnet/what-is-aspnet-core)
- [Entity Framework Core 5.0](https://docs.microsoft.com/en-us/ef/core/)

### Prerequisites

- Make sure you are running on the latest .NET 5 SDK (SDK 5.0 and above only). [Get the latest one here.](https://dotnet.microsoft.com/download/dotnet/5.0)

- Latest [Visual Studio Code](https://code.visualstudio.com) or [Visual Studio 2019](https://visualstudio.microsoft.com) above.

- Install the latest [.NET & EF CLI Tools](https://docs.microsoft.com/en-us/ef/core/cli/dotnet) by using this command :

    ```.NET Core CLI
    dotnet tool install --global dotnet-ef
    ```

### 💫 Features Included

- Permissions Management based on Role Claims
- ASP.NET Core Identity
- Includes Sample CRUD Controllers
- Activity Logging for Entity Framework Core
- Serilog
- Automapper
- JWT & Refresh Tokens
- Swagger
- Nice npm scripts for help the dotnet commands and more

## Usage

- Just download the required packages:

    ```.NET Core CLI
    dotnet restore
    ```

- Then you can use helper scripts in package.json

    ```.NET Core CLI
    npm run start
    ```

- You're ready! 🤟
  
## 🛣️ Roadmap

See the [open issues](issues-url) for a list of proposed features (and known issues).

- ~~Fix typos on this Readme, or prepare a better one.~~
- Ensure the code quality.

## 👷 Contributing

Contributions are what make the open-source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Open a Merge Request

## License

Distributed under the GNU GPLv3 License. See `LICENSE` for more information.

## Contact

### REMMSOFT Inc

- Blogs at [remmsoft.com](https://www.remmsoft.com/blog.html)
- LinkedIn - [REMMSOFT](https://www.linkedin.com/company/remmsoft/)

## 🤜 🤛 Support

Has this Project helped you learn something New? or Helped you at work? Do Consider Supporting. Here are a few ways by which you can support.

- Leave a star! ⭐️
- Recommend this awesome project to your colleagues.

<a href="https://www.buymeacoffee.com/codewithmukesh" target="_blank"><img src="https://opensource.org/files/osi_standard_logo_0.png" alt="Buy Me A Coffee" width="200"  style="height: 220px !important;width: 190px !important;" ></a>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/1?style=flat-square
[contributors-url]: https://gitlab.com/remmsoft-shared/nextapi-net5/-/project_members
[forks-shield]: https://img.shields.io/1?style=flat-square
[forks-url]: https://gitlab.com/remmsoft-shared/nextapi-net5/-/forks
[stars-shield]: https://img.shields.io/badge/dynamic/json?color=green&label=gitlab%20stars&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F28354407
[stars-url]: https://gitlab.com/remmsoft-shared/nextapi-net5/-/starrers
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/company/remmsoft/
