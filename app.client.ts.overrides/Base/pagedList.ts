﻿/* Auto Generated */

export interface PagedList<T> {
  dataCount: number;
  data: T[];
}
