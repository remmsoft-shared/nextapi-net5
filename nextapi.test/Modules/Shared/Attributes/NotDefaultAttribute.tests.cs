using System;
using com.shared;
using Xunit;

namespace com.test
{
    public class NotDefaultTests
    {
        [Fact]
        public void WhenEmpty_NotValid_Test()
        {
            var validator = new NotDefaultAttribute();

            // Guid Test
            var isValidGuid = validator.IsValid(Guid.Empty);

            // Date Test
            var isValidDate = validator.IsValid(new DateTime(0));

            Assert.False(isValidGuid); // Fails if you use != instead of !value.Equals(defaultValue)
            Assert.False(isValidDate); // Fails if you use != instead of !value.Equals(defaultValue)
        }
    }

}