using System;
using System.Collections.Generic;
using com.data;
using Xunit;

namespace com.test
{
    public class BaseTestFixture : IDisposable
    {
        private readonly EntityUnitofWork uow;

        public BaseTestFixture(EntityUnitofWork uow)
        {
            this.uow = uow;

            Seed();
        }

        private void Seed()
        {
            var scl = new TestSeedCreatorLibrary();

            // var cx = uow.Repository<Entity>().Context;
            // cx.Database.EnsureDeleted();
            // cx.Database.EnsureCreated();

            uow.SaveChanges();
        }

        public void Dispose()
        {

        }
    }

    [CollectionDefinition("BaseTestCollection", DisableParallelization = true)]
    public class BaseTestCollection : ICollectionFixture<BaseTestFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}