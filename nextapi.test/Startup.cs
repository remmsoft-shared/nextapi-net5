﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using AutoMapper;
using System.Net.Mail;
using com.data;
using com.service;
using com.service.identity;
using com.data.identity;
using com.shared;

namespace com.test
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options =>
                options.UseInMemoryDatabase("AppTestDb")
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning)));

            var apiAssembly = Assembly.Load(new AssemblyName("nextapi"));
            services.AddAutoMapper(apiAssembly);


            services.AddTransient<SmtpClient>((serviceProvider) =>
            {
                return new SmtpClient();
            });

            // services
            //   .AddTransient<ServiceResolver>(serviceProvider => key =>
            //   {
            //       //var type = Assembly.GetAssembly(typeof(ServiceResolver)).GetType($"I{key}Service");
            //       //var instance = serviceProvider.GetService(type);
            //       //return instance as IBaseService;
            //       switch (key)
            //       {
            //           case "ServiceName":
            //               return serviceProvider.GetService<ServiceObj>();
            //           default:
            //               throw new KeyNotFoundException(); // or maybe return null, up to you
            //       }
            //   });

            services
               .AddSingleton<ITenantProvider>(t =>
               {
                   var tenantProvider = new TenantProvider();

                   tenantProvider.SetSlug("0000");
                   tenantProvider.SetUserId(AdminUser.Id);
                   tenantProvider.SetUserName(AdminUser.Name);

                   return tenantProvider;
               });

            services
                .AddSingleton<IConfigurationRoot>(r =>
                {
                    return new ConfigurationBuilder()
                     .AddJsonFile("./appsettings.test.json")
                     .Build();
                });

            services
                .AddTransient(typeof(EntityUnitofWork))
                .AddTransient<IEmailService, EmailService>()
                ;

            // Identity
            services
                .AddScoped<IIdentityService, IdentityService>()
                .AddTransient(typeof(AppIdentityUnitofWork))
                .AddTransient<IUserSessionService, UserSessionService>()
                .AddTransient<IUserService, UserService>()
            ;

        }
    }
}
