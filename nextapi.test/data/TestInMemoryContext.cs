
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using com.data;
using com.shared;

namespace com.test
{
    public static class TestInMemoryContext
    {
        public static EntityUnitofWork GenerateUow(ITenantProvider tenantProvider)
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase("AppTestDb")
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;

            AppDbContext testContext = new AppDbContext(options, tenantProvider);
            testContext.Database.EnsureDeleted();
            testContext.Database.EnsureCreated();

            EntityUnitofWork result = new EntityUnitofWork(testContext);
            return result;
        }
    }


}