﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using com.shared.domain;
using com.service;
using com.shared;

namespace com.controller
{
    public class RsBaseCrudController<A, U, G, S> : RsBaseController
        where U : EntityUpdatePayload
        where G : EntityGetResult
        where S : IRsBaseCrudService<A, U, G>
    {
        protected S service;
        public RsBaseCrudController(S service)
        {
            this.service = service;
        }
        [Route("Add"), HttpPost]
        public virtual async Task<ActionResult<ApiResult>> Add(A model)
        {
            //Check validation errors
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //Generic olarak gelen servisin Add metoduna ilgili dto ve
            //token bilgisini okuyarak elde edilen, User ID gönderilmiştir.
            var result = await service.Add(model);

            //Kayıt işleminin sonucunu kontrol eder.
            if (result.Message != Messages.Ok)
                return BadRequest(result);

            return Ok(result);
        }
        [Route("Update"), HttpPut]
        public virtual async Task<ActionResult<ApiResult>> Update(U model)
        {
            //Check validation errors
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //Generic olan gelen servisin Update metoduna ilgili view model 
            //ve token bilgisini okuyarak elde edilen User ID gönderilmiştir.
            var result = await service.Update(model);

            //Update işleminin sonucunu kontrol eder.
            if (result.Message != Messages.Ok)
                return BadRequest(result);

            return Ok(result);
        }
        [Route("Delete"), HttpDelete]
        public virtual async Task<ActionResult<ApiResult>> Delete(Guid id)
        {
            //Generic olan gelen servisin Delete metoduna ilgili id
            //ve token bilgisini okuyarak elde edilen User ID gönderilmiştir.
            var result = await service.Delete(id);

            //Delete işleminin sonucunu kontrol eder.
            if (result.Message != Messages.Ok)
                return BadRequest(result);

            return Ok(result);
        }

        [Route("GetById"), HttpGet]
        public virtual async Task<ActionResult<G>> GetById(Guid id)
        {
            var result = await service.GetById(id);

            if (result == null)
                return BadRequest(Messages.GNE0001);

            return Ok(result);
        }
    }
}
