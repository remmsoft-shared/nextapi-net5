﻿using System;
using System.Threading.Tasks;
using com.service.identity;
using com.shared;
using com.shared.domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace com.controller
{

    [ApiController]
    [Authorize]
    public class IdentityController : ControllerBase
    {
        private readonly IIdentityService identityService;
        private readonly IHttpContextAccessor accessor;

        public IdentityController(
            IIdentityService identityService,
            IHttpContextAccessor accessor)
        {
            this.identityService = identityService;
            this.accessor = accessor;
        }

        [Route("Token"), HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<TokenResult>> Token([FromBody] TokenPayload payload)
        {
            var clientIp = accessor?.HttpContext?.GetRemoteIPAddress()?.MapToIPv4().ToString();
            var userAgent = accessor?.HttpContext?.GetUserAgent();

            if (clientIp == null || userAgent == null)
                return BadRequest();

            TokenResult res = await identityService.Token(payload, clientIp, userAgent);
            if (string.IsNullOrEmpty(res.Token))
            {
                return BadRequest(res.ErrMessage);
            }
            else
            {
                return Ok(res);
            }
        }

        [Route("Register"), HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterPayload payload)
        {
            var clientIp = accessor?.HttpContext?.GetRemoteIPAddress()?.MapToIPv4().ToString();
            var userAgent = accessor?.HttpContext?.GetUserAgent();

            if (clientIp == null || userAgent == null)
                return BadRequest();

            //Check validation errors
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            TokenResult res = await identityService.Register(payload, clientIp, userAgent);
            
            if (string.IsNullOrEmpty(res.Token))
            {
                return BadRequest(res.ErrMessage);
            }
            else
            {
                return Ok(res);
            }
        }

        [Route("Logout"), HttpPost]
        public async Task<IActionResult> Logout()
        {
            if (accessor == null || accessor.HttpContext == null)
                return Ok();

            var userName = IdentityClaims.UserName(accessor.HttpContext);
            var claims = IdentityClaims.UserData(accessor.HttpContext);

            if (userName != null && claims.SessionKey != null)
                await identityService.Logout(userName, claims.SessionKey.Value);

            return Ok();
        }
        
        [Route("RefreshToken"), HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<RefreshTokenResult>> RefreshToken([FromBody] RefreshTokenPayload payload)
        {
            // var claims = IdentityClaims.UserData(this.HttpContext);
            var result = await identityService.RefreshToken(payload);
            return Ok(result);
        }

        [Route("RevokeToken"), HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RevokeToken()
        {
            if (accessor == null || accessor.HttpContext == null)
                return Ok();

            var userName = IdentityClaims.UserName(accessor.HttpContext);
            if (userName != null)
            {
                var result = await identityService.RevokeToken(userName);

                if (result.Message == Messages.Ok)
                    return Ok();
                else 
                    return BadRequest();
            } 
            else 
            {
                return BadRequest();
            }
           
        }

        [Route("resetpassword"), HttpPost]
        public async Task<IActionResult> ResetPassword(IdentityResetPasswordPayload payload)
        {
            IdentityResetPasswordResult res = await identityService.ResetPassword(payload);
            if (!res.Succeeded)
            {
                return BadRequest(res.ErrMessage);
            }
            else
            {
                return Ok();
            }
        }

        [Route("changepassword"), HttpPost]
        public async Task<IActionResult> ChangePassword(IdentityChangePasswordPayload payload)
        {
            var userId = IdentityClaims.UserId(this.HttpContext);

            if (userId == null)
                return BadRequest();

            IdentityChangePasswordResult res = await identityService.ChangePassword(payload, userId);
            if (!res.Succeeded)
            {
                return BadRequest(res.ErrMessage);
            }
            else
            {
                return Ok();
            }
        }

    }
}
