﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Threading.Tasks;
using System.Threading;
using com.data.entity;
using com.shared;

namespace com.data
{
    public class AppDbContext : DbContext
    {
        private readonly ITenantProvider tenantProvider;

        public AppDbContext(
            DbContextOptions<AppDbContext> options, ITenantProvider tenantProvider) : base(options)
        {
            this.tenantProvider = tenantProvider;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            #region Set global filters
            // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯        
            modelBuilder.SetQueryFilterOnAllEntities<IRsBaseEntity>(p => !p.IsDeleted);
            // ________________________________
            #endregion

            foreach (IMutableEntityType entity in modelBuilder.Model.GetEntityTypes())
            {
                entity.SetTableName(entity.DisplayName());
            }

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            AppDbContextFluentApi.Create(modelBuilder);

            Seed.SeedData(modelBuilder);
        }

        public override int SaveChanges()
        {
            var userId = this.tenantProvider.GetUserId();
            ChangeTracker.DetectChanges();
            ChangeTracker.ProcessModification(userId, this);
            ChangeTracker.ProcessCreation(userId, this);

            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var userId = this.tenantProvider.GetUserId();
            ChangeTracker.DetectChanges();
            ChangeTracker.ProcessModification(userId, this);
            ChangeTracker.ProcessCreation(userId, this);

            return (await base.SaveChangesAsync(true, cancellationToken));
        }


        #region Queries
        // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
        // public DbSet<StringResult> StringQuery { get; set; }
        // public DbSet<GuidResult> IdQuery { get; set; }
        // public DbSet<NumberResult> NumberQuery { get; set; }
        // public DbSet<DoubleResult> DoubleQuery { get; set; }
        // ________________________________
        #endregion


        // public virtual DbSet<SysAutoCode> SysAutoCode { get; set; }

    }
}
