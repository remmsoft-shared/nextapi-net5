﻿using System;
using System.IO;
using com.shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace com.data
{
    public class AppDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {

        /// <summary>
        /// args [0] ==> Slug
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public AppDbContext CreateDbContext(string[] args)
        {

#if DEBUG
            var configPath = Directory.GetCurrentDirectory();
            IConfigurationRoot configuration = new ConfigurationBuilder()
                                            .SetBasePath(configPath)
                                            .AddJsonFile("appsettings.Development.json")
                                            .Build();
#else
            // Prod yayın yapıldığında appsettings e erişim yolu değiştiriliyor.
            IConfigurationRoot configuration = new ConfigurationBuilder()
                                            .AddJsonFile("./appsettings.json")
                                            .Build();
#endif

            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            var connectionString = configuration.GetConnectionString("App");
            optionsBuilder.UseSqlServer(connectionString,
                opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds)
                );

            var tenantProvider = new TenantProvider();
            if (args != null && args.Length > 0)
            {
                tenantProvider.SetSlug(args[0]);
                tenantProvider.SetUserId(new Guid(args[1]));
            }

            return new AppDbContext(optionsBuilder.Options, tenantProvider);
        }
    }
}
