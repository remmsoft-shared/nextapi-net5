﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using com.data.entity;

namespace com.data
{

    public interface IAppEntityRepository<T>
    where T : RsBaseEntity
    {
        AppDbContext Context { get; set; }
        Task<T> GetById(Guid id, bool isDeleted = false);
        IQueryable<T> Get(bool isDeleted = false);
        IQueryable<T> Query(bool isDeleted = false);
        void Add(T entity);
        void AddRange(List<T> entity);
    }

    public class AppEntityRepository<T> : IAppEntityRepository<T>
          where T : RsBaseEntity
    {
        private AppDbContext con;
        public AppEntityRepository(AppDbContext context)
        {
            con = context;
        }
        public AppDbContext Context
        {
            get { return con; }
            set { con = value; }

        }
        public virtual async Task<T> GetById(Guid id, bool isDeleted = false)
        {
            return await con.Set<T>().
                Where(x => x.Id == id && x.IsDeleted == isDeleted).
                FirstOrDefaultAsync();
        }
        public IQueryable<T> Get(bool isDeleted = false)
        {
            return con.Set<T>().Where(x => x.IsDeleted == isDeleted).AsQueryable();
        }
        public IQueryable<T> Query(bool isDeleted = false)
        {
            return con.Set<T>().AsNoTracking().Where(x => x.IsDeleted == isDeleted);
        }
        public virtual void Add(T entity)
        {
            con.Set<T>().Add(entity);
        }
        public virtual void AddRange(List<T> entity)
        {
            con.Set<T>().AddRange(entity);
        }

    }
}