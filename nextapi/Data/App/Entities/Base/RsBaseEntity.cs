﻿using System;

namespace com.data.entity
{
    public interface IRsBaseEntity
    {
        Guid Id { get; set; }
        bool IsDeleted { get; set; }
    }

    public class RsBaseEntity : IRsBaseEntity
    {
        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
