﻿using System;

namespace com.data.entity
{
    public interface IRsBaseTableEntity : IRsBaseEntity
    {
        DateTime CreateDT { get; set; }
        DateTime? UpdateDT { get; set; }
        Guid CreateBy { get; set; }
        Guid? UpdateBy { get; set; }
    }

    public class RsBaseTableEntity : RsBaseEntity, IRsBaseTableEntity
    {
        public DateTime CreateDT { get; set; }
        public DateTime? UpdateDT { get; set; }
        public Guid CreateBy { get; set; }
        public Guid? UpdateBy { get; set; }
    }
}
