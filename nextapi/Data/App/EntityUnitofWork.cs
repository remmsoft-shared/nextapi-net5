﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.data.entity;

namespace com.data
{
    public class EntityUnitofWork : IDisposable
    {
        public readonly AppDbContext con;
        public EntityUnitofWork(AppDbContext con)
        {
            this.con = con;
        }
        private readonly Dictionary<Type, object> repositories = new Dictionary<Type, object>();
        public IAppEntityRepository<T> Repository<T>() where T : RsBaseEntity
        {
            if (repositories.Keys.Contains(typeof(T)))
            {
                return (repositories[typeof(T)] as IAppEntityRepository<T>) ?? new AppEntityRepository<T>(con);
            }
            IAppEntityRepository<T> repository = new AppEntityRepository<T>(con);
            repositories.Add(typeof(T), repository);
            return repository;
        }
        public virtual async Task<int> SaveChangesAsync()
        {
            return await con.SaveChangesAsync();
        }
        public virtual int SaveChanges()
        {
            return con.SaveChanges();
        }
        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    con.Dispose();
                }
            }
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
