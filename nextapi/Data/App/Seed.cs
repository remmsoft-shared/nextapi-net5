﻿using System;
using Microsoft.EntityFrameworkCore;
using System.IO;

namespace com.data
{
    public static class Seed
    {
        public static void SeedData(ModelBuilder modelBuilder)
        {
            // Guid entityId = Guid.Parse("f63212d3-c2e9-4da7-9cf6-c0a094608ce2");

            // modelBuilder.Entity<Entity>().HasData(
            //     new Entity
            //     {
            //         Id = entityId,
                    
            //     }
            // );
            
        }

        public static void GenerateStoredProcedures(AppDbContext cx)
        {
            var storedProceduresDir = Path.Combine(AppContext.BaseDirectory, "StoredProcedures");
            if (Directory.Exists(storedProceduresDir) == false)
                return;

            var storedProcedures = Directory.GetFiles(storedProceduresDir);
            if (storedProcedures == null || storedProcedures.Length < 1)
                return;

            foreach (var storedProcedure in storedProcedures)
            {
                var sql = "";
                var lines = File.ReadAllLines(storedProcedure);
                if (lines == null || lines.Length < 1)
                    continue;

                foreach (var line in lines)
                {
                    if (line.Trim() == "GO")
                    {
                        cx.Database.ExecuteSqlRaw(sql);
                        sql = "";
                    }
                    else if (string.IsNullOrWhiteSpace(line.Trim()))
                    {
                        continue;
                    }
                    else
                    {
                        sql += line + Environment.NewLine;
                    }

                }

            }
        }

    }
}
