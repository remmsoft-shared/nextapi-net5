﻿using com.data.entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;

namespace com.data
{
    public static class ChangeTrackerExtensions
    {
        public static void ProcessCreation(this ChangeTracker changeTracker, Guid authUserId, AppDbContext cx)
        {
            foreach (var item in changeTracker.Entries<IRsBaseTableEntity>().Where(e => e.State == EntityState.Added))
            {
                item.Entity.CreateDT = DateTime.UtcNow;
                item.Entity.CreateBy = authUserId;
            }
        }

        public static void ProcessModification(this ChangeTracker changeTracker, Guid authUserId, AppDbContext cx)
        {
            foreach (var item in changeTracker.Entries<IRsBaseTableEntity>().Where(e => e.State == EntityState.Modified))
            {
                item.Entity.UpdateDT = DateTime.UtcNow;
                item.Entity.UpdateBy = authUserId;
            }
        }

    }
}
