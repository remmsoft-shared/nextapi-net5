using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using com.shared.domain;

namespace com.data
{

    public static class ExtensionMethods
    {
        public static async Task<object> InvokeAsync(this MethodInfo @this, object obj, params object[] parameters)
        {
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
            dynamic awaitable = @this.Invoke(obj, parameters);
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
#pragma warning disable 8602
            await awaitable;
#pragma warning restore 8602
#pragma warning disable CS8602 // Dereference of a possibly null reference.
            return awaitable.GetAwaiter().GetResult();
#pragma warning restore CS8602 // Dereference of a possibly null reference.
        }
    }

    public static class Extensions
    {
      

        //Skip metoduna ek olarak, Skip değeri 0 gelirse, parametre olarak gelen listeyi olduğu gibi geri döndürür.
        public static IQueryable<TEntity> SkipOrAll<TEntity>(this IQueryable<TEntity> src, int? skip)
        {
            return skip.HasValue ? src.Skip(skip.Value) : src;
        }

        //Take metoduna ek olarak, Take değeri 0 gelirse, parametre olarak gelen listeyi olduğu gibi geri döndürür.
        public static IQueryable<TEntity> TakeOrAll<TEntity>(this IQueryable<TEntity> src, int? take)
        {
            return take.HasValue ? src.Take(take.Value) : src;
        }

        /// <summary>
        /// Dinamik LINQ sorgusu oluşturmak için extension yazıldı.
        /// Kullanıcının sıralamak istediği kolonu dinamik olarak sıralar.
        /// </summary>
        /// <param name="source">Sıralanmak üzere gelen liste. Herhangi DTO türünde olabilir.</param>
        /// <param name="orderByProperty">Sıralarken dikkate alınacak alan ismi.</param>
        /// <param name="sortDirection">Azalan-Artan ölçüde sıralamak için kullanılır. Descending:-1 Ascending:1</param>
        /// <typeparam name="TEntity">Generic IQueryable DTO nesnesi</typeparam>
        /// <returns>İlgili DTO alanına göre sıralanmış IQueryable nesne döndürür.</returns>
        /// 
        public static IQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> source, string orderByProperty,
                          SortBy sortDirection)
        {
            //Order için field alanı verilmezse, uygulamanın patlamaması için gelen listeyi doğrudan geri döndürür.
            if (string.IsNullOrWhiteSpace(orderByProperty))
                return source;

            string command = sortDirection == SortBy.OrderByDescending ? "OrderByDescending" : "OrderBy";

            var property = typeof(TEntity).GetProperty(UppercaseFirst(orderByProperty), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            if (property == null)
                return source;

            var parameter = Expression.Parameter(typeof(TEntity));
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new[] { typeof(TEntity), property.PropertyType },
                                          source.Expression, Expression.Quote(orderByExpression));
            return source.Provider.CreateQuery<TEntity>(resultExpression);
        }

        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

    }
}
