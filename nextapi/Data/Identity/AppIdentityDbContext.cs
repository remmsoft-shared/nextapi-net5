using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace com.data.identity
{
    public class AppIdentityDbContext : IdentityDbContext<RsIdentityUser, RsIdentityRole, Guid>
    {
        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> options)
            : base(options)
        {

        }


        public virtual DbSet<UserSession> UserSession { get; set; }

    }
}
