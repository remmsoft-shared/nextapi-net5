﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace com.data.identity
{
    public class AppIdentityDbContextFactory : IDesignTimeDbContextFactory<AppIdentityDbContext>
    {

        public AppIdentityDbContext CreateDbContext(string[] args)
        {

#if DEBUG
            var configPath = Directory.GetCurrentDirectory();
            IConfigurationRoot configuration = new ConfigurationBuilder()
                                            .SetBasePath(configPath)
                                            .AddJsonFile("appsettings.Development.json")
                                            .Build();
#else
            // Prod yayın yapıldığında appsettings e erişim yolu değiştiriliyor.
            IConfigurationRoot configuration = new ConfigurationBuilder()
                                            .AddJsonFile("./appsettings.json")
                                            .Build();
#endif

            var optionsBuilder = new DbContextOptionsBuilder<AppIdentityDbContext>();
            var connectionString = configuration.GetConnectionString("App");
            optionsBuilder.UseSqlServer(connectionString);

            return new AppIdentityDbContext(optionsBuilder.Options);
        }
    }
}
