﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace com.data.identity
{
     public interface IAppIdentityEntityRepository<T>
        where T : RsBaseIdentityEntity
    {
        AppIdentityDbContext Context { get; set; }
        Task<T> GetById(Guid id, bool isDeleted = false);
        IQueryable<T> Get(bool isDeleted = false);
        IQueryable<T> Query(bool isDeleted = false);
        void Add(T entity);
        void AddRange(List<T> entity);
        void RemoveRange(List<Guid> payload);
        void Remove(T entity);
    }

    public class AppIdentityEntityRepository<T> : IAppIdentityEntityRepository<T>
          where T : RsBaseIdentityEntity
    {
        private AppIdentityDbContext con;
        public AppIdentityEntityRepository(AppIdentityDbContext context)
        {
            con = context;
        }
        public AppIdentityDbContext Context
        {
            get { return con; }
            set { con = value; }

        }
        public virtual async Task<T> GetById(Guid id, bool isDeleted = false)
        {
            return await con.Set<T>().
                Where(c => c.Id == id).
                FirstOrDefaultAsync();
        }
        public IQueryable<T> Get(bool isDeleted = false)
        {
            return con.Set<T>().AsQueryable();
        }
        public IQueryable<T> Query(bool isDeleted = false)
        {
            return con.Set<T>().AsNoTracking();
        }
        public virtual void Add(T entity)
        {
            con.Set<T>().Add(entity);
        }
        public virtual void AddRange(List<T> entity)
        {
            con.Set<T>().AddRange(entity);
        }
        public virtual void RemoveRange(List<Guid> payload)
        {
            var recs = con.Set<T>().Where(c => payload.Contains(c.Id)).ToList();

            con.Set<T>().RemoveRange(recs);
        }
        public virtual void Remove(T entity)
        {
            con.Set<T>().Remove(entity);
        }
    }
}