﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.data.identity
{
    public class AppIdentityUnitofWork : IDisposable
    {
        private AppIdentityDbContext con;
        public AppIdentityUnitofWork(AppIdentityDbContext con)
        {
            this.con = con;
        }

        private Dictionary<Type, object> repositories = new Dictionary<Type, object>();
        public IAppIdentityEntityRepository<T> Repository<T>() where T : RsBaseIdentityEntity
        {
            if (repositories.Keys.Contains(typeof(T)))
            {
                return (repositories[typeof(T)] as IAppIdentityEntityRepository<T>) ?? new AppIdentityEntityRepository<T>(con);
            }
            IAppIdentityEntityRepository<T> repository = new AppIdentityEntityRepository<T>(con);
            repositories.Add(typeof(T), repository);
            return repository;
        }

        public virtual async Task<int> SaveChangesAsync()
        {
            return await con.SaveChangesAsync();
        }

        public virtual int SaveChanges()
        {
            return con.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    con.Dispose();
                }
            }
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
