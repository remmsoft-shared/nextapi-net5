﻿using System;

namespace com.data.identity
{
    public interface IRsBaseIdentityEntity
    {
        Guid Id { get; set; }
    }
    public class RsBaseIdentityEntity : IRsBaseIdentityEntity
    {
        public Guid Id { get; set; }
    }
}
