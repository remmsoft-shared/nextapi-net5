using System;
using Microsoft.AspNetCore.Identity;

namespace com.data.identity
{
    public class RsIdentityUser : IdentityUser<Guid>
    {
        public DateTime? LastLoginDate { get; set; }
        public string? RefreshToken { get; set; }
        public DateTime? RefreshTokenEndDate { get; set; }
    }
}