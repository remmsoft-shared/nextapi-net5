﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace com.data.identity
{
    [Table("AspNetUserSession")]
    public class UserSession : RsBaseIdentityEntity
    {
        public string UserName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ClientIp { get; set; }
        public string UserAgent { get; set; }
        public Guid SessionKey { get; set; }
    }
}