using System;
using com.service.identity;
using com.shared;
using Microsoft.AspNetCore.Mvc.Filters;

namespace com.controller
{
    public class TenantActionFilter : IActionFilter
    {
        private readonly ITenantProvider tenantProvider;

        public TenantActionFilter(
                ITenantProvider tenantProvider
        )
        {
            this.tenantProvider = tenantProvider;
        }


        public void OnActionExecuting(ActionExecutingContext context)
        {
            string? userId = IdentityClaims.UserId(context.HttpContext);
            string? userName = IdentityClaims.UserName(context.HttpContext);

            this.tenantProvider.SetUserId(userId == null ? Guid.Empty : new Guid(userId));
            this.tenantProvider.SetUserName(userName ?? "");
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // Do something after the action executes.
        }
    }
}