using com.data;
using com.data.identity;
using com.service.identity;
using com.shared;
using Microsoft.Extensions.DependencyInjection;

namespace com.service
{
    public static class AppDependencyInjection
    {
        public static void ConfigureServices(IServiceCollection services) 
        {
            services
                .AddTransient(typeof(EntityUnitofWork))
                .AddTransient<IEmailService, EmailService>()
                .AddSingleton<ITenantProvider, TenantProvider>()
                ;

            // Identity
            services
                .AddScoped<IIdentityService, IdentityService>()
                .AddTransient(typeof(AppIdentityUnitofWork))
                .AddTransient<IUserSessionService, UserSessionService>()
                .AddTransient<IUserService, UserService>()
            ;
        }
    }

}