﻿using AutoMapper;
using com.data;
using com.data.entity;
using com.shared;
using com.shared.domain;
using System;
using System.Threading.Tasks;

namespace com.service
{

    public interface IRsBaseCrudService<A, U, G> :IRsBaseService
        where U : EntityUpdatePayload
        where G : EntityGetResult
    {
        Task<G> GetById(Guid id);
        Task<ApiResult> Add(A model, bool isCommit = true);
        Task<ApiResult> Update(U model, bool isCommit = true);
        Task<ApiResult> Delete(Guid id, bool isCommit = true);
    }

    public class RsBaseCrudService<A, U, G, D> : IRsBaseCrudService<A, U, G>
        where D : RsBaseEntity
        where U : EntityUpdatePayload
        where G : EntityGetResult
    {
        protected EntityUnitofWork uow;
        protected readonly IMapper mapper;

        public RsBaseCrudService(EntityUnitofWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.mapper = mapper;
        }
        public virtual D AddMapping(A model)
        {
            D entity = mapper.Map<D>(model);
            return entity;
        }
        public virtual void UpdateMapping(U model, D entity)
        {
            mapper.Map(model, entity);
        }
        public virtual async Task<ApiResult> Add(A model, bool isCommit = true)
        {
            D entity = AddMapping(model);

            if (entity.Id.IsNullOrEmpty())
                entity.Id = Guid.NewGuid();

            uow.Repository<D>().Add(entity);

            if (isCommit)
                await uow.SaveChangesAsync();

            return new ApiResult { Data = entity.Id, Message = Messages.Ok };
        }
        public virtual async Task<ApiResult> Update(U model, bool isCommit = true)
        {
            D entity = await uow.Repository<D>().GetById(model.Id);

            if (entity == null)
                return new ApiResult() { Data = model.Id, Message = Messages.GNE0001 };

            UpdateMapping(model, entity);

            if (isCommit)
                await uow.SaveChangesAsync();

            return new ApiResult() { Data = entity.Id, Message = Messages.Ok };
        }
        public virtual async Task<ApiResult> Delete(Guid id, bool isCommit = true)
        {
            D entity = await uow.Repository<D>().GetById(id);

            if (entity == null)
                return new ApiResult() { Data = id, Message = Messages.GNE0001 };

            entity.IsDeleted = true;

            if (isCommit)
                await uow.SaveChangesAsync();

            return new ApiResult() { Data = id, Message = Messages.Ok };
        }

        public virtual async Task<G> GetById(Guid id)
        {
            D entity = await uow.Repository<D>().GetById(id);
            throw new NotImplementedException();
        }
    }

}
