﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using com.shared;
using com.shared.domain;
using Microsoft.Extensions.Configuration;

namespace com.service
{
    public interface IEmailService
    {
        void SendMail(EmailPayload model, bool isBodyHtml = true);
        void SendMailBasicTemplate(EmailBasicTemplatePayload model, string? templatePath = null);
        void SendTempPassword(SendTempPasswordPayload payload);
        void ResetPassword(ResetPasswordPayload payload);
    }

    public class EmailService : IEmailService
    {

        private readonly SmtpClient _smtpClient;
        private readonly IConfiguration _configuration;
        public EmailService(SmtpClient smtpClient, IConfiguration configuration)
        {
            _smtpClient = smtpClient;
            _configuration = configuration;
        }

        public void SendMail(EmailPayload model, bool isBodyHtml = true)
        {
            bool isMailSystemActive = _configuration.GetValue<bool>("Email:IsActive");
            if (!isMailSystemActive)
                return;

            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(_configuration.GetValue<string>("Email:Smtp:From"));

                if ((model.To == null || model.To.Count == 0) && (model.Cc == null || model.Cc.Count == 0) && (model.Bcc == null || model.Bcc.Count == 0))
                    return;

                if (model.To != null && model.To.Count > 0)
                    foreach (var to in model.To)
                        mailMessage.To.Add(new MailAddress(to));

                if (model.Cc != null && model.Cc.Count > 0)
                    foreach (var cc in model.Cc)
                        mailMessage.CC.Add(new MailAddress(cc));

                if (model.Bcc != null && model.Bcc.Count > 0)
                    foreach (var bcc in model.Bcc)
                        mailMessage.Bcc.Add(new MailAddress(bcc));

                mailMessage.Subject = model.Subject;
                mailMessage.Body = model.Body;
                mailMessage.IsBodyHtml = isBodyHtml;

                _smtpClient.Send(mailMessage);
            }
            catch
            {
                throw;
            }
        }

        public void SendMailBasicTemplate(EmailBasicTemplatePayload model, string? templatePath = null)
        {
            if ( model == null || (model.To == null || model.To.Count == 0) && (model.Cc == null || model.Cc.Count == 0) && (model.Bcc == null || model.Bcc.Count == 0))
                return;

            string template;
            if (templatePath == null)
                templatePath = "BasicTemplate.html";

#if DEBUG
            var configPath = Path.Combine(Directory.GetCurrentDirectory(), "Assets", "Email");
            template = File.ReadAllText(Path.Combine(configPath, templatePath));
#else
      template = File.ReadAllText(Path.Combine("./Assets/Email", templatePath));
#endif


            template = template.Replace("**BackgroundColor**", model.BackgroundColor);
            template = template.Replace("**Header**", model.Header);
            template = template.Replace("**Content**", model.Content);
            template = template.Replace("**TableHeader**", model.TableHeader);
            template = template.Replace("**TableContent**", model.TableContent);

            EmailPayload emailPayload = new EmailPayload
            {
                To = model?.To ?? new List<string>(),
                Cc = model?.Cc ?? new List<string>(),
                Bcc = model?.Bcc ?? new List<string>(),
                Subject = model?.Subject ?? "",
                Body = template
            };

            SendMail(emailPayload, true);
        }

        public void SendTempPassword(SendTempPasswordPayload payload)
        {
            var templatePath = System.IO.File.ReadLines(Path.Combine("./Assets/Email", "UserRegistirationTemplate.html")).ToList();

            var feBaseUrl = _configuration.GetValue<string>("AppSettings:FEBaseURL");

            var subjectLine = templatePath[0];
            subjectLine = subjectLine.Replace("<!-- {SUBJECT:", "");
            subjectLine = subjectLine.Replace("} -->", "");

            var body = string.Concat(templatePath);
            body = body.Replace("{userName}", payload.UserName);
            body = body.Replace("{password}", payload.Password);
            body = body.Replace("{url}", feBaseUrl);

            var emailPayload = new EmailPayload();

            emailPayload.To = new List<string> { payload.UserEmail };
            emailPayload.Subject = subjectLine;
            emailPayload.Body = body;

            this.SendMail(emailPayload);
        }
        public void ResetPassword(ResetPasswordPayload payload)
        {
            var templatePath = System.IO.File.ReadLines(Path.Combine("./Assets/Email", "ResetPasswordTemplate.html")).ToList();

            var subjectLine = templatePath[0];
            subjectLine = subjectLine.Replace("<!-- {SUBJECT:", "");
            subjectLine = subjectLine.Replace("} -->", "");

            var body = string.Concat(templatePath);
            body = body.Replace("{password}", payload.Password);

            var emailDto = new EmailPayload();

            emailDto.To = new List<string> { payload.UserEmail };
            emailDto.Subject = subjectLine;
            emailDto.Body = body;

            this.SendMail(emailDto);
        }
    }
}
