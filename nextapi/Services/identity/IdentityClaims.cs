﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Text.Json;
using com.shared.domain;

namespace com.service.identity
{
    public static class IdentityClaims
    {
        public static string? UserName(HttpContext context)
        {
            return context?.User?.Identity?.Name;
        }

        public static string? UserId(HttpContext context)
        {
            Claim? userIdClaim = context.User.Claims.FirstOrDefault(c => c.Type == "userId");
            return userIdClaim?.Value;
        }

        public static CustomClaimResult UserData(HttpContext context)
        {
            Claim? userDataClaim = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.UserData);
            if (userDataClaim == null || userDataClaim.Value == null)
                return new CustomClaimResult();
            else 
                return JsonSerializer.Deserialize<CustomClaimResult>(userDataClaim.Value) ?? new CustomClaimResult();
        }
    }
}
