﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using com.data;
using com.data.identity;
using com.shared.domain;
using com.shared;

namespace com.service.identity
{

    public interface IIdentityService
    {
        Task<TokenResult> Token(TokenPayload payload, string clientIp, string userAgent);
        Task<TokenResult> Register(RegisterPayload payload, string clientIp, string userAgent);
        Task<ApiResult> Logout(string userName, Guid sessionKey);
        Task<IdentityResetPasswordResult> ResetPassword(IdentityResetPasswordPayload payload);
        Task<IdentityChangePasswordResult> ChangePassword(IdentityChangePasswordPayload payload, string userId);
        Task<RefreshTokenResult?> RefreshToken(RefreshTokenPayload payload);
        Task<ApiResult> RevokeToken(string username);
    }

    public class IdentityService : IIdentityService
    {
        private readonly SignInManager<RsIdentityUser> signInManager;
        private readonly EntityUnitofWork uow;
        private readonly UserManager<RsIdentityUser> userManager;
        private readonly IConfiguration configuration;
        private readonly IUserSessionService userSessionService;
        private readonly IUserService userService;
        private readonly IEmailService emailService;
        private readonly AppIdentityUnitofWork iuow;

        public IdentityService(
            EntityUnitofWork uow,
            UserManager<RsIdentityUser> userManager,
            SignInManager<RsIdentityUser> signInManager,
            IConfiguration configuration,
            IUserSessionService userSessionService,
            IUserService userService,
            IEmailService emailService,
            AppIdentityUnitofWork iuow
            )
        {
            this.uow = uow;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
            this.userSessionService = userSessionService;
            this.userService = userService;
            this.emailService = emailService;
            this.iuow = iuow;
        }

        public async Task<TokenResult> Token(TokenPayload payload, string clientIp, string userAgent)
        {
            try
            {
                RsIdentityUser? appUser = userManager.Users.SingleOrDefault(r => r.UserName == payload.UserName);
                if (appUser == null)
                    return new TokenResult() { Token = "", ErrMessage = Messages.AUE0001 };

                #region Session control
                // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯       
                bool IsUserAvaibleForSession = await userSessionService.IsUserAvaibleForSession(payload.UserName);
                // ________________________________
                #endregion


                string password = payload.Password;

                Microsoft.AspNetCore.Identity.SignInResult result = await signInManager.CheckPasswordSignInAsync(appUser, password, false);

                if (!result.Succeeded)
                    return new TokenResult() { Token = null, ErrMessage = Messages.AUE0001 };

                var sessionKey = Guid.NewGuid();

                var sessionResult = await userSessionService.ForceOpenNewSessionForUser(payload.UserName, clientIp, userAgent, sessionKey);
                if (sessionResult.Message != Messages.Ok)
                    return new TokenResult() { Token = "", ErrMessage = sessionResult.Message };

                #region Claims
                // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
             
                List<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, appUser.UserName));
                claims.Add(new Claim("userId", appUser.Id.ToString()));

                CustomClaimResult customClaims = new CustomClaimResult 
                {
                    SessionKey = sessionKey
                };
                claims.Add(new Claim(ClaimTypes.UserData, JsonSerializer.Serialize(customClaims)));

                // ________________________________
                #endregion

                #region Token
                // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
                // authentication successful so generate jwt token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(configuration.GetValue<string>("AppSettings:JWT_Secret"));

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.Now.AddHours(5),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                    NotBefore = DateTime.Now
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var resToken = tokenHandler.WriteToken(token);
                var refreshToken = CreateRefreshToken();
                // ________________________________
                #endregion

                TokenResult tokenResult = new TokenResult
                {
                    Token = resToken,
                    UserName = appUser.UserName,
                    ErrMessage = null,
                    RefreshToken = refreshToken,
                    EMail = appUser.Email,
                };

                appUser.RefreshToken = refreshToken;
                appUser.RefreshTokenEndDate = DateTime.Now.AddHours(24);
                await iuow.SaveChangesAsync();

                await userService.UpdateLoginDate(appUser.Id);
                return tokenResult;
            }
            catch (Exception ex)
            {
                return new TokenResult() { Token = null, ErrMessage = ex.Message };
            }
        }

        public async Task<TokenResult> Register(RegisterPayload payload, string clientIp, string userAgent)
        {
            RsIdentityUser existUser = await userManager.FindByNameAsync(payload.UserName);
            if (existUser != null)
            {
                return new TokenResult { Token = null, ErrMessage = Messages.AUE0002 };
            }

            var user = new RsIdentityUser()
            {
                UserName = payload.UserName,
                Email = payload.Email
            };

            var userCreateResult = await userManager.CreateAsync(user, payload.Password);

            if (userCreateResult.Succeeded) 
            {
                var tokenPayload = new TokenPayload 
                {
                    UserName = payload.UserName,
                    Password = payload.Password
                };
                return await this.Token(tokenPayload, clientIp, userAgent);
            }
            else 
            {
                return new TokenResult { Token = null, ErrMessage = Messages.GNE0002 };
            }
        }

        public async Task<RefreshTokenResult?> RefreshToken(RefreshTokenPayload payload)
        {
            var cx = iuow.Repository<UserSession>().Context;

            var principal = GetPrincipalFromExpiredToken(payload.Token);
            var username = principal?.Identity?.Name;

            var user = cx.Users.SingleOrDefault(u => u.UserName == username);
            if (user == null || user.RefreshToken != payload.RefreshToken) return null;

            var newJwtToken = GenerateAccessToken(principal?.Claims);
            var newRefreshToken = CreateRefreshToken();

            user.RefreshToken = newRefreshToken;
            await cx.SaveChangesAsync();

            return new RefreshTokenResult
            {
                Token = newJwtToken,
                RefreshToken = newRefreshToken
            };
        }

        public async Task<ApiResult> RevokeToken(string username)
        {
            var cx = iuow.Repository<UserSession>().Context;

            var user = cx.Users.SingleOrDefault(u => u.UserName == username);
            
            if (user == null)
                return new ApiResult() { Data = null, Message = Messages.GNE0001 };
             
            user.RefreshToken = null;

            await cx.SaveChangesAsync();

            return new ApiResult() { Data = null, Message = Messages.Ok };

        }

        private string GenerateAccessToken(IEnumerable<Claim>? claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["AppSettings:JWT_Secret"]));

            var jwtToken = new JwtSecurityToken(
                claims: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(5),
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
            );

            return new JwtSecurityTokenHandler().WriteToken(jwtToken);
        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var jwtSecret = configuration.GetValue<string>("AppSettings:JWT_Secret");
            var key = Encoding.ASCII.GetBytes(jwtSecret);

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }

        public async Task<ApiResult> Logout(string userName, Guid sessionKey)
        {
            return await userSessionService.LogoutSession(userName, sessionKey);
        }

        public async Task<IdentityResetPasswordResult> ResetPassword(IdentityResetPasswordPayload payload)
        {
            try
            {
                RsIdentityUser user = await userManager.FindByNameAsync(payload.Username);

                if (user != null)
                {
                    var tempPassword = StringHelper.GetRandomPassword();

                    string resetToken = await userManager.GeneratePasswordResetTokenAsync(user);
                    IdentityResult res = await userManager.ResetPasswordAsync(user, resetToken, tempPassword);
                    if (res.Succeeded)
                    {
                        var mailPayload = new ResetPasswordPayload
                        {
                            UserEmail = user.Email,
                            Password = tempPassword
                        };
                        emailService.ResetPassword(mailPayload);
                        return new IdentityResetPasswordResult { Succeeded = res.Succeeded, ErrMessage = null };
                    }
                    else
                    {
                        return new IdentityResetPasswordResult { Succeeded = false, ErrMessage = "GNE0004" };
                    }
                }
                else
                {
                    return new IdentityResetPasswordResult { Succeeded = false, ErrMessage = "GNE0001" };
                }
            }
            catch (Exception ex)
            {
                return new IdentityResetPasswordResult { Succeeded = false, ErrMessage = ex.Message };
            }
        }

        public async Task<IdentityChangePasswordResult> ChangePassword(IdentityChangePasswordPayload payload, string userId)
        {
            try
            {
                RsIdentityUser user = await userManager.FindByIdAsync(userId);

                if (user != null)
                {
                    IdentityResult res = await userManager.ChangePasswordAsync(user, payload.CurrentPassword, payload.NewPassword);

                    if (res.Succeeded)
                        return new IdentityChangePasswordResult { Succeeded = res.Succeeded, ErrMessage = null };
                    else
                        return new IdentityChangePasswordResult { Succeeded = false, ErrMessage = "GNE0004" };
                }
                else
                {
                    return new IdentityChangePasswordResult { Succeeded = false, ErrMessage = "GNE0001" };
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string CreateRefreshToken()
        {
            byte[] number = new byte[32];
            using (RandomNumberGenerator random = RandomNumberGenerator.Create())
            {
                random.GetBytes(number);
                return Convert.ToBase64String(number);
            }
        }
    }
}
