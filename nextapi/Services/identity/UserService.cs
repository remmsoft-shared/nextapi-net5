using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using com.shared.domain;
using com.shared;
using com.data.identity;
using com.data;

namespace com.service.identity
{

    public interface IUserService
    {
        Task<IdentityCreateResult> Create(IdentityCreatePayload payload);
        Task<IdentityUpdateResult> Update(IdentityUpdatePayload payload);
        Task<IdentityDeleteResult> Delete(Guid id);
        Task<AppUserGetResult> GetById(Guid id);
        Task<IdentityResult> AddRoles(Guid userId, IEnumerable<Guid> roles);
        Task<IdentityResult> RemoveRoles(Guid userId, IEnumerable<Guid> roles);
        Task<PagedList<AppUserGetResult>> Get(AppUserGetPayload payload);
        Task<int> GetActiveUserCount();
        Task UpdateLoginDate(Guid userId);
    }

    public class UserService : IUserService
    {
        private readonly SignInManager<RsIdentityUser> _signInManager;
        private readonly UserManager<RsIdentityUser> userManager;
        private readonly IConfiguration configuration;
        private readonly EntityUnitofWork uow;
        private readonly AppIdentityUnitofWork iuow;
        public readonly IEmailService EmailService;

        public UserService(
            UserManager<RsIdentityUser> userManager,
            SignInManager<RsIdentityUser> signInManager,
            IConfiguration configuration,
            EntityUnitofWork uow,
            AppIdentityUnitofWork iuow,
            IEmailService emailService
            )
        {
            this.userManager = userManager;
            _signInManager = signInManager;
            this.configuration = configuration;
            EmailService = emailService;
            this.uow = uow;
            this.iuow = iuow;
        }
        public async Task<IdentityUpdateResult> Update(IdentityUpdatePayload payload)
        {
            RsIdentityUser entity = await userManager.FindByIdAsync(payload.Id);

            if (entity == null)
                return new IdentityUpdateResult() { Id = null, ErrMessage = Messages.GNE0001 };

            entity.UserName = payload.UserName;
            entity.NormalizedUserName = payload.UserName.ToUpper();
            entity.Email = payload.Email;

            await userManager.UpdateAsync(entity);

            return new IdentityUpdateResult() { Id = entity.Id, ErrMessage = null };
        }

        public async Task<IdentityCreateResult> Create(IdentityCreatePayload payload)
        {
            try
            {
                RsIdentityUser existUser = await userManager.FindByNameAsync(payload.UserName);
                if (existUser != null)
                {
                    return new IdentityCreateResult() { Id = null, ErrMessage = Messages.EME0002 };
                }

                var user = new RsIdentityUser()
                {
                    UserName = payload.UserName,
                    Email = payload.Email
                };

                IdentityResult result;

                if (payload.IsExternalUser)
                {
                    result = await userManager.CreateAsync(user);
                }
                else
                {
                    //Payload'ın şifre kısmı rastgele belirlenir.
                    var tempPassword = StringHelper.GetRandomPassword();
                    result = await userManager.CreateAsync(user, tempPassword);

                    if (result.Succeeded)
                    {
                        var mailPayload = new SendTempPasswordPayload
                        {
                            UserEmail = payload.Email,
                            UserName = payload.UserName,
                            Password = tempPassword
                        };
                        EmailService.SendTempPassword(mailPayload);
                    }
                }


                if (result.Succeeded == false)
                {
                    return new IdentityCreateResult() { Id = null, ErrMessage = Messages.GNE0002 };
                }

                return new IdentityCreateResult() { Id = user.Id, ErrMessage = null };
            }
            catch (Exception ex)
            {
                return new IdentityCreateResult() { Id = null, ErrMessage = ex.Message };
            }
        }

        public async Task<IdentityDeleteResult> Delete(Guid id)
        {
            try
            {
                var user = await userManager.FindByIdAsync(id.ToString());
                IdentityResult identityResult = await userManager.DeleteAsync(user);

                return new IdentityDeleteResult { Succeeded = identityResult.Succeeded, ErrMessage = null };
            }
            catch (Exception ex)
            {
                return new IdentityDeleteResult { Succeeded = false, ErrMessage = ex.Message };
            }
        }

        public async Task<IdentityResult> AddRoles(Guid userId, IEnumerable<Guid> roles)
        {
            if (userId.IsNullOrEmpty() || roles == null || roles.Count() == 0)
                return new IdentityResult();

            RsIdentityUser user = await userManager.FindByIdAsync(userId.ToString());

            if (user != null)
            {
                var rolPayload = roles.Select(r => r.ToString()).ToList();
                return await userManager.AddToRolesAsync(user, rolPayload);
            }
            else
            {
                return new IdentityResult();
            }

        }

        public async Task<IdentityResult> RemoveRoles(Guid userId, IEnumerable<Guid> roles)
        {
            if (userId.IsNullOrEmpty() || roles == null || roles.Count() == 0)
                return new IdentityResult();

            RsIdentityUser user = await userManager.FindByIdAsync(userId.ToString());

            if (user != null)
            {
                var rolPayload = roles.Select(r => r.ToString()).ToList();
                return await userManager.RemoveFromRolesAsync(user, rolPayload);
            }
            else
            {
                return new IdentityResult();
            }

        }

        public async Task<AppUserGetResult> GetById(Guid id)
        {
            var user = await userManager.FindByIdAsync(id.ToString());

            return new AppUserGetResult
            {
                Id = user.Id,
                Email = user.Email,
                UserName = user.UserName,
                LastLoginDate = user.LastLoginDate
            };
        }

        public async Task<PagedList<AppUserGetResult>> Get(AppUserGetPayload payload)
        {

            var icx = this.iuow.Repository<UserSession>().Context;

            var query = (
                from u in icx.Users
                where payload.Text == null
                    || u.UserName.Contains(payload.Text)
                    || u.Email.Contains(payload.Text)
                select new AppUserGetResult
                {
                    Id = u.Id,
                    Email = u.Email,
                    UserName = u.UserName,
                    IsOnline = (
                        from s in icx.UserSession
                        where s.EndDate == null
                            && s.UserName == u.UserName
                        select s
                    ).Any(),
                    LastLoginDate = u.LastLoginDate
                }
            );

            var resultList = await query.
                OrderBy(payload.SortField, payload.SortOrder).
                SkipOrAll(payload.First).
                TakeOrAll(payload.Rows).
                ToListAsync();

            return new PagedList<AppUserGetResult>
            {
                DataCount = query.Count(),
                Data = (resultList == null || resultList.Count == 0) ? new List<AppUserGetResult>() : resultList
            };

        }

        public async Task<int> GetActiveUserCount()
        {
            var icx = this.iuow.Repository<UserSession>().Context;
            var result = await (
                from s in icx.UserSession
                where s.EndDate == null
                group s by s.UserName into gr
                select gr.Key
            ).CountAsync();

            return result;
        }

        public async Task UpdateLoginDate(Guid userId)
        {
            RsIdentityUser user = await userManager.FindByIdAsync(userId.ToString());

            if (user != null)
            {
                user.LastLoginDate = DateTime.Now;
                await userManager.UpdateAsync(user);
            }

        }
    }
}
