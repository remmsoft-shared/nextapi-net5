using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using com.data.identity;
using com.data;
using com.shared.domain;
using com.shared;

namespace com.service.identity
{

    public interface IUserSessionService
    {
        Task<bool> ValidateSession(string userName, Guid sessionKey);
        Task<bool> IsUserAvaibleForSession(string userName);
        Task<ApiResult> ForceOpenNewSessionForUser(string userName, string clientIp, string userAgent, Guid sessionKey);
        Task<ApiResult> LogoutSession(string userName, Guid sessionKey);
        Task<PagedList<UserSessionGetResult>> Get(UserSessionGetPayload payload);
    }

    public class UserSessionService : IUserSessionService
    {

        protected AppIdentityUnitofWork uow;
        public UserSessionService(AppIdentityUnitofWork uow)
        {
            this.uow = uow;
        }

        public async Task<bool> ValidateSession(string userName, Guid sessionKey)
        {
            return await uow.Repository<UserSession>().Query()
                 .Where(c => c.UserName == userName && c.EndDate == null && c.SessionKey == sessionKey)
                 .AnyAsync();
        }

        public async Task<bool> IsUserAvaibleForSession(string userName)
        {
            var userSession = await uow.Repository<UserSession>().Query()
                .Where(c => c.UserName == userName && c.EndDate == null)
                .FirstOrDefaultAsync();

            if (userSession == null)
                return true;
            else
                return false;
        }

        public async Task<ApiResult> ForceOpenNewSessionForUser(string userName, string clientIp, string userAgent, Guid sessionKey)
        {
            var userSessions = await uow.Repository<UserSession>().Get()
                .Where(c => c.UserName == userName && c.EndDate == null)
                .ToListAsync();

            foreach (var userSession in userSessions)
            {
                userSession.EndDate = DateTime.Now;
            }

            await uow.SaveChangesAsync();

            var newUserSession = new UserSession
            {
                ClientIp = clientIp,
                UserAgent = userAgent,
                UserName = userName,
                StartDate = DateTime.Now,
                SessionKey = sessionKey
            };
            uow.Repository<UserSession>().Add(newUserSession);

            try
            {
                await uow.SaveChangesAsync();
                return new ApiResult(newUserSession.Id, Messages.Ok);
            }
            catch (Exception)
            {
                return new ApiResult(null, Messages.GNE0002);
            }

        }

        public async Task<ApiResult> LogoutSession(string userName, Guid sessionKey)
        {
            var activeSession = await uow.Repository<UserSession>().Get()
                .Where(c => c.UserName == userName && c.EndDate == null && c.SessionKey == sessionKey)
                .FirstOrDefaultAsync();

            if (activeSession != null)
            {
                activeSession.EndDate = DateTime.Now;
                await uow.SaveChangesAsync();
            }

            return new ApiResult(null, Messages.Ok);
        }
        public async Task<PagedList<UserSessionGetResult>> Get(UserSessionGetPayload payload)
        {
            var icx = uow.Repository<UserSession>().Context;

            var query = (
                from log in icx.UserSession
                where EF.Functions.DateDiffDay(payload.StartDate, log.StartDate) >= 0
                    && (payload.EndDate == null || EF.Functions.DateDiffDay(payload.EndDate, log.StartDate) <= 0)
                    && (payload.UserName == null || log.UserName == payload.UserName)
                select log
            ).AsQueryable();

            var resultList = await GetMapping(query).
                            OrderBy(payload.SortField, payload.SortOrder).
                            SkipOrAll(payload.First).
                            TakeOrAll(payload.Rows).
                            ToListAsync();

            return new PagedList<UserSessionGetResult>
            {
                DataCount = query.Count(),
                Data = (resultList == null || resultList.Count == 0) ? new List<UserSessionGetResult>() : resultList
            };
        }

        private IQueryable<UserSessionGetResult> GetMapping(IQueryable<UserSession> query)
        {
            return (
                from q in query
                select new UserSessionGetResult
                {
                    Id = q.Id,
                    UserName = q.UserName,
                    StartDate = q.StartDate,
                    EndDate = q.EndDate
                });
        }
    }

}
