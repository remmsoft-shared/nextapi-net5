﻿using System;

namespace com.shared
{
    public static class AdminUser
    {
        public static readonly Guid Id = Guid.Parse("7cbf9971-7957-48dd-8198-3394a9bf0059");
        public static readonly string Name = "Admin";
        public static readonly string EMail = "info@remmsoft.com";
    }
}
