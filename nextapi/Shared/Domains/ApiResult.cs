﻿namespace com.shared.domain
{
    public class ApiResult
    {
        public object? Data { get; set; }
        public string Message { get; set; }

        public ApiResult()
        {

        }

        public ApiResult(object? data, string message)
        {
            Data = data;
            Message = message;
        }
    }
}
