﻿using System;
using System.ComponentModel.DataAnnotations;

namespace com.shared.domain
{
    public class EntityGetResult
    {
        [Required]
        public Guid Id { get; set; }
    }
}
