﻿using System;
using System.ComponentModel.DataAnnotations;

namespace com.shared.domain
{
    public class EntityUpdatePayload
    {
        [Required]
        public Guid Id { get; set; }
    }
}
