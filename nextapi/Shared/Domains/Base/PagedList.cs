using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace com.shared.domain
{

    public class PagedList<T>
    {
        public int DataCount { get; set; }
        [Required]
        public IList<T> Data { get; set; }
    }
}
