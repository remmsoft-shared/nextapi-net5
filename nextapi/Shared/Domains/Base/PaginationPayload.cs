
namespace com.shared.domain
{
    public class PaginationPayload
    {
        public int? First { get; set; }
        public int? Rows { get; set; }
        public SortBy SortOrder { get; set; }
        public string SortField { get; set; }
    }
}
