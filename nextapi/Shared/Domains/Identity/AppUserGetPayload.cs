

namespace com.shared.domain
{

    public class AppUserGetPayload : PaginationPayload
    {
        public string? Text { get; set; }
    }

}