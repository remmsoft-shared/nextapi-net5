

using System;

namespace com.shared.domain
{

    public class AppUserGetResult : EntityGetResult
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public bool IsOnline { get; set; }
    }

}
