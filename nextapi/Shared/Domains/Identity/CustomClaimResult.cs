

using System;

namespace com.shared.domain
{

    public class CustomClaimResult
    {
        public Guid? SessionKey { get; set; }
    }

}
