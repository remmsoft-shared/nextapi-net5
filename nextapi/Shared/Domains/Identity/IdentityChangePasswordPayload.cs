
namespace com.shared.domain
{

    public class IdentityChangePasswordPayload
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}