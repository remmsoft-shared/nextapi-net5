

using System;
using System.ComponentModel.DataAnnotations;


namespace com.shared.domain
{

    public class IdentityCreatePayload
    {
        [Required]
        public string UserName { get; set; }
        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public Guid? PerId { get; set; }
        [MaxLength(2)]
        public string Lang { get; set; }
        public bool IsExternalUser { get; set; }
    }

}