

using System;


namespace com.shared.domain
{

    public class IdentityCreateResult
    {
        public Guid? Id { get; set; }
        public string? ErrMessage { get; set; }
    }

}