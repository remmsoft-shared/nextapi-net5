
namespace com.shared.domain
{

    public class IdentitySucceedResult
    {
        public bool Succeeded { get; set; }
        public string? ErrMessage { get; set; }
    }

}