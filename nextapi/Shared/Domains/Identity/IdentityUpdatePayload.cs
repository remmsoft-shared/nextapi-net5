using System;
using System.ComponentModel.DataAnnotations;


namespace com.shared.domain
{

    public class IdentityUpdatePayload
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        public Guid? PerId { get; set; }
        [MaxLength(2)]
        public string Lang { get; set; }
    }
}