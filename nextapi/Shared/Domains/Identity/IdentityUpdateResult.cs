using System;


namespace com.shared.domain
{

    public class IdentityUpdateResult
    {
        public Guid? Id { get; set; }
        public string? ErrMessage { get; set; }
    }

}