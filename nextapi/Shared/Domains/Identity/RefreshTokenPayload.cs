
namespace com.shared.domain
{
    public class RefreshTokenPayload
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}