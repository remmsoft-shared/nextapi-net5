
namespace com.shared.domain
{
    public class RefreshTokenResult
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}