using System.ComponentModel.DataAnnotations;


namespace com.shared.domain
{

    public class RegisterPayload
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required, MinLength(4)]
        public string Password { get; set; }

    }

}