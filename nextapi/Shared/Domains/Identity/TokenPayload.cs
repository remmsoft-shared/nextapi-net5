using System.ComponentModel.DataAnnotations;


namespace com.shared.domain
{

    public class TokenPayload
    {
        [Required]
        public string UserName { get; set; }

        [Required, MinLength(4)]
        public string Password { get; set; }

    }

}