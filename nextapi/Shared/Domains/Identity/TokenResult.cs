

using System;

namespace com.shared.domain
{

    public class TokenResult
    {
        public string? EMail { get; set; }
        public string? RefreshToken { get; set; }
        public string? Token { get; set; }
        public string UserName { get; set; }
        public string? ErrMessage { get; set; }
    }

}