using System;

namespace com.shared.domain
{

    public class UserSessionGetPayload : PaginationPayload
    {
        public string? UserName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

}
