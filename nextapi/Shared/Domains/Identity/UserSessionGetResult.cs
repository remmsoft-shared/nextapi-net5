using System;
using System.ComponentModel.DataAnnotations;

namespace com.shared.domain
{

    public class UserSessionGetResult
    {
        [Required]
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

}
