﻿using System.Collections.Generic;

namespace com.shared.domain
{
    public class EmailBasicTemplatePayload : EmailPayload, IEmailPayload
    {
        public string BackgroundColor { get; set; }
        public string Header { get; set; }
        public string Content { get; set; }
        public string TableHeader { get; set; }
        public string TableContent { get; set; }
    }


}
