﻿using System.Collections.Generic;

namespace com.shared.domain
{

    public class EmailPayload : IEmailPayload
    {
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public List<string> Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public EmailPayload()
        {
            To = new List<string>();
            Cc = new List<string>();
            Bcc = new List<string>();
        }
    }


}
