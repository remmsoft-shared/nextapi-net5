﻿using System.Collections.Generic;

namespace com.shared.domain
{

    public interface IEmailPayload
    {
        List<string> To { get; set; }
        List<string> Cc { get; set; }
        List<string> Bcc { get; set; }
        string Subject { get; set; }
        string Body { get; set; }
    }


}
