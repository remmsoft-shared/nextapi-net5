﻿using System.Collections.Generic;

namespace com.shared.domain
{
    
    public class ResetPasswordPayload
    {
        public string UserEmail { get; set; }
        public string Password { get; set; }
    }

}
