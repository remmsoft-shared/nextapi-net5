﻿using System.Collections.Generic;

namespace com.shared.domain
{

    public class SendTempPasswordPayload
    {
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string Password { get; set; }
    }

}
