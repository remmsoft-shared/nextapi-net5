using System;
using System.Globalization;

namespace com.shared
{
    public static class DateTimeHelper
    {
        public static byte GetWeekNumber(DateTime date)
        {
            return (byte)CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
        }

        public static byte GetDayOfWeek(DateTime date)
        {
            var dayOfWeek = (byte)date.DayOfWeek;

            // Sunday = 0, DayofWeek.Monday so Sunday = 7
            return dayOfWeek == 0 ? (byte)7 : dayOfWeek;
        }

        /// <summary>
        /// EndDate - StartDate
        /// </summary>
        public static int GetMonthDifference(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (endDate.Year - startDate.Year) + endDate.Month - startDate.Month;
            return monthsApart;
        }

    }
}