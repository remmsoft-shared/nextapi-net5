using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace com.shared
{
    public static class HttpContextExtensions
    {
        /// <summary>
        /// Get remote ip address, optionally allowing for x-forwarded-for header check
        /// </summary>
        /// <param name="context">Http context</param>
        /// <param name="allowForwarded">Whether to allow x-forwarded-for header check</param>
        /// <returns>IPAddress</returns>
        public static IPAddress? GetRemoteIPAddress(this HttpContext context, bool allowForwarded = true)
        {
            if (allowForwarded)
            {
                string? header = (context.Request.Headers["CF-Connecting-IP"].FirstOrDefault() ?? context.Request.Headers["X-Forwarded-For"].FirstOrDefault());
                if (IPAddress.TryParse(header, out IPAddress? ip))
                {
                    return ip;
                }
            }
            return context?.Connection.RemoteIpAddress;
        }

        /// <summary>
        /// Get user agent
        /// </summary>
        /// <param name="context">Http context</param>
        /// <returns>string</returns>
        public static string? GetUserAgent(this HttpContext context)
        {
            string? userAgent = context.Request.Headers["User-Agent"].FirstOrDefault();

            return userAgent;
        }
    }

}