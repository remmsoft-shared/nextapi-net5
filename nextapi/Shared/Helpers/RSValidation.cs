﻿using System;

namespace com.shared
{
    public static class RSValidation
    {
        public static bool IsNullOrEmpty(this Guid? guid)
        {
            return (guid == Guid.Empty || guid == null);
        }
        public static bool IsNullOrEmpty(this Guid guid)
        {
            return (guid == Guid.Empty);
        }
        public static bool IsValidEmail(this string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;

            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
