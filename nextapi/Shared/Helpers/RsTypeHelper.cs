
namespace com.shared
{
    public static class RsTypeHelper
    {

        public static object? GetPropertyValue(object obj, string propName)
        {
            return obj?.GetType().GetProperty(propName)?.GetValue(obj, null);
        }

    }
}