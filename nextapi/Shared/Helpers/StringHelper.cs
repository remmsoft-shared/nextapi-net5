using System.IO;

namespace com.shared
{
    public class StringHelper
    {
        public static string GetRandomPassword()
        {
            return Path.GetRandomFileName().Replace(".", "").Substring(0, 8);
        }

        public static bool InRange(int index, string[] array)
        {
            return (index >= 0) && (index < array.Length);
        }
    }
}