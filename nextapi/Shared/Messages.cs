﻿namespace com.shared
{
    /// <summary>
    /// Sistem mesajları 7 karakter olarak tasarlanmıştır:
    /// Ilk 2 karakter Modül Kodu
    /// 3. Karakter Mesaj Kodu (E:Error, S:Success, I:Information, W:Warning)
    /// Son 4 karakter hata kodu
    /// </summary>
    public static class Messages
    {
      
        // Modul kodu : 'GN'
        #region General
        // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯

        /// <summary>
        /// İşlem başarılı
        /// - Status Code: Ok
        /// </summary>
        public static string Ok = "Ok";

        /// <summary>
        /// Kayıt bulunamadı 
        /// - Status Code: NotFound
        /// </summary>
        public static string GNE0001 = "GNE0001";

        /// <summary>
        ///  Kayıt sırasında hata oluştu.
        /// - Status Code: BadRequest
        /// </summary>
        public static string GNE0002 = "GNE0002";

        // ________________________________
        #endregion General

        // Modul kodu : 'EM'
        #region EMail
            
        /// <summary>
        ///  Kullanıcıya mail gönderilemedi
        /// - Status Code: InternalServerError
        /// </summary>
        public static string EME0001 = "EME0001";

        /// <summary>
        ///  Bu email ile daha önce bir kayıt oluşturulmuş.
        /// - Status Code: BadRequest
        /// </summary>
        public static string EME0002 = "EME0002";

        #endregion


        // Modul kodu : 'AU'
        #region Auth
            
        /// <summary>
        /// Yanlış kullanıcı adı ya da parola!
        /// </summary>
        public static string AUE0001 = "AUE0001";
  
        /// <summary>
        /// Bu kullanıcı adı ile daha önce bir kayıt oluşturulmuş.
        /// </summary>
        public static string AUE0002 = "AUE0002";

        #endregion

    }
}
