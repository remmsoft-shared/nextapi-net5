
using System;

namespace com.shared
{
    public interface ITenantProvider
    {
        string GetSlug();
        void SetSlug(string value);

        Guid GetUserId();
        void SetUserId(Guid value);

        string GetUserName();
        void SetUserName(string value);
    }

    public class TenantProvider : ITenantProvider
    {
        private string _slug;
        private string userName;
        private Guid userId;
        
        public void SetSlug(string slug)
        {
            _slug = slug;
        }

        public string GetSlug()
        {
            return _slug;
        }

        public void SetUserId(Guid value)
        {
            this.userId = value;
        }

        public Guid GetUserId()
        {
            return this.userId;
        }

        public void SetUserName(string value)
        {
            this.userName = value;
        }

        public string GetUserName()
        {
            return this.userName;
        }
    }
}