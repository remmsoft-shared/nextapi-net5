﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using com.data;
using com.data.identity;
using com.service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace com.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("Policy", builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                );
            });

            services.AddControllers()
                    .AddJsonOptions(o =>
                            {
                                o.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                                o.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                            });

            
            services.AddTransient<SmtpClient>((serviceProvider) =>
                        {
                            var config = serviceProvider.GetRequiredService<IConfiguration>();

                            bool mailIsActive = config.GetValue<Boolean>("Email:IsActive");
                            string mailUserName = config.GetValue<string>("Email:Smtp:Username");

                            if (!mailIsActive || string.IsNullOrEmpty(mailUserName))
                            {
                                return new SmtpClient();
                            }

                            return new SmtpClient()
                            {
                                Host = config.GetValue<string>("Email:Smtp:Host"),
                                Port = config.GetValue<int>("Email:Smtp:Port"),
                                EnableSsl = config.GetValue<bool>("Email:Smtp:EnableSsl"),
                                UseDefaultCredentials = config.GetValue<bool>("Email:Smtp:UseDefaultCredentials"),
                                Credentials = new NetworkCredential(
                                        config.GetValue<string>("Email:Smtp:Username"),
                                        config.GetValue<string>("Email:Smtp:Password")
                                    )
                            };
                        });

            services.AddHttpContextAccessor();
            
            Assembly apiAssembly = Assembly.GetExecutingAssembly();
            services.AddAutoMapper(apiAssembly);

            #region Auth 
            // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
             services.AddDbContext<AppIdentityDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("App")));

            services
                .AddIdentity<RsIdentityUser, RsIdentityRole>()
                .AddEntityFrameworkStores<AppIdentityDbContext>()
                .AddDefaultTokenProviders();

            var jwtSecret = Configuration.GetValue<string>("AppSettings:JWT_Secret");
            var key = Encoding.ASCII.GetBytes(jwtSecret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 4;
                options.Password.RequiredUniqueChars = 0;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = true;
            });

            // ___________________________________________________
            #endregion

            #region Data    
            // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("App"),
                    opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds)
                ));
            // ___________________________________________________
            #endregion

            #region Service
            // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
            services.AddControllersWithViews()
                .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            AppDependencyInjection.ConfigureServices(services);
            // ___________________________________________________
            #endregion


            #region SWAGGER
            // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯        
            services.AddSwaggerGen(c =>
                        {
                            c.SwaggerDoc("v1", new OpenApiInfo { Title = "NextApi", Version = "v0.1" });
                            c.AddSecurityDefinition("Bearer",
                            new OpenApiSecurityScheme
                            {
                                In = ParameterLocation.Header,
                                Description = "Please enter into field the word 'Bearer' following by space and JWT",
                                Name = "Authorization",
                                Type = SecuritySchemeType.ApiKey,
                                Scheme = "Bearer"
                            });
                            c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                            {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header

                        },
                        new List<string>()
                    }
                            });
                        });
            // ________________________________
            #endregion
            
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto | ForwardedHeaders.All;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            UserManager<RsIdentityUser> userManager,
            RoleManager<RsIdentityRole> roleManager)
        {
             app.UseForwardedHeaders();

            // app.UseMiddleware<MultiTenantMiddleWare>();

            // Angular 7 için XHR isteği filtresi
            app.Use(async (ctx, next) =>
            {
                await next();
                if (ctx.Response.StatusCode == 204
                    || ctx.Response.StatusCode == 205
                    || ctx.Response.StatusCode == 304)
                {
                    ctx.Response.ContentLength = 0;
                }
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }


            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());


            #region SWAGGER
            // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯        
            app.UseSwagger(c =>
                        {
                            c.RouteTemplate = "swagger/{documentName}/swagger.json";
                        });

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "NextApi v0.1");
                c.RoutePrefix = "apidoc";
                c.InjectStylesheet("/Assets/swagger-ui/custom.css");
                c.InjectJavascript("/Assets/swagger-ui/custom.js");
            });
            // ________________________________
            #endregion

            Assembly apiAssembly = Assembly.GetExecutingAssembly();

            #region Identity
            // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯     
            app.UseAuthentication();

            using (var serviceScope = app.ApplicationServices
            .GetRequiredService<IServiceScopeFactory>()
            .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<AppIdentityDbContext>())
                {
                    //#if !DEBUG
                    if (context != null)
                        IdentitySeed.SeedData(userManager, roleManager, Configuration, context);
                    //#endif
                    // context.Database.Migrate();
                }
            }
            // ________________________________
            #endregion

            #region Data
            // ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯     
            using (var serviceScope = app.ApplicationServices
            .GetRequiredService<IServiceScopeFactory>()
            .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<AppDbContext>())
                {
                    //#if !DEBUG
                    if (context != null)
                        Seed.GenerateStoredProcedures(context);
                    //#endif

                    // Şu an için riskli daha sonra deneyebiliriz
                    // context.Database.Migrate();
                }
            }
            // ___________________________________________________
            #endregion

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                        Path.Combine(Directory.GetCurrentDirectory(), "Assets")),
                RequestPath = "/Assets"
            });
            
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                // For SSL
                endpoints.MapGet("/.well-known/acme-challenge/{id}", async context =>
                {
                    var id = context.Request.RouteValues["id"];
                    await context.Response.WriteAsync($"{id}");
                });
            });

        }
    }
}
